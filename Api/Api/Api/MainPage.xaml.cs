﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Api
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Get_Definition(object sender, EventArgs e)
        {
            var client = new HttpClient();
            string word = text_entry.Text;
            var spaceApiAddress = "https://owlbot.info/api/v2/dictionary/" + word;
            var uri = new Uri(spaceApiAddress);

            List_of_words definitionData = new List_of_words();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                definitionData = JsonConvert.DeserializeObject<List_of_words>(jsonContent);

                
                BindingContext = definitionData;
            }
            //numberLabel.Text = "Number of people in space " + spaceData.Number.ToString();
            //spaceListView.ItemsSource = new ObservableCollection<Person>(spaceData.People);
            
        }
    }
    }

